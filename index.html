<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<title>Linked data for relational minds</title>

		<link rel="stylesheet" href="dist/reset.css">
		<link rel="stylesheet" href="dist/reveal.css">
		<link rel="stylesheet" href="dist/theme/white.css">

		<!-- Theme used for syntax highlighting of code -->
		<link rel="stylesheet" href="plugin/highlight/monokai.css" id="highlight-theme">

		<!-- Printing and PDF exports -->
		<script>
			var link = document.createElement( 'link' );
			link.rel = 'stylesheet';
			link.type = 'text/css';
			link.href = window.location.search.match( /print-pdf/gi ) ? 'css/print/pdf.css' : 'css/print/paper.css';
			document.getElementsByTagName( 'head' )[0].appendChild( link );
		</script>
    <style>
.reveal h1, .reveal h2, .reveal h3, .reveal h4 {
  text-transform: none;
}
.reveal .smaller {
  font-size: 70%;
}
li code {
  color: blue;
}
.reveal h1 {
  font-size: 2.0em;
}
    </style>
	</head>
	<body vocab="http://schema.org/" typeof="PresentationDigitalDocument">
		<div class="reveal">
			<div class="slides">
        <section>
          <h1 property="name">Getting started with Wikidata and SPARQL: build a dynamic web page</h1>
          <h2>A SWIB22 workshop</h2>
          <div property="author" typeof="Person" resource="https://dscott.ca/#i">
            <p><span property="name">Dan Scott</span> &lt;<a property="url" href="https://dscott.ca/#i">https://dscott.ca/#i</a>&gt;</p>
            <p class="smaller"><span property="jobTitle">Associate Librarian</span>, <span property="worksFor" typeof="CollegeOrUniversity" resource="https://laurentian.ca/"><a property="url" href="https://laurentian.ca/"><span property="name">Laurentian University</span></a></span></p>
          </div>
          <p><span property="dateCreated datePublished">2022-11-30</span> <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/" style="padding-left: 2em;"><img alt="Creative Commons License" style="border-width:0; vertical-align: middle" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png"></a>
          </p>
        </section>
        <section>
          <section>
            <h3>Web of documents</h3>
            <ul>
              <li>A web address (like <a href="https://laurentian.ca/">https://laurentian.ca/</a>)</li>
              <li>… returns an HTML document -- a web page</li>
              <li>… which may link to other web addresses</li>
            </ul>
          </section>
          <section>
            <h3>Web of data</h3>
            <ul>
              <li>Not just documents that have meaning for humans</li>
              <li>… but also data that has meaning for machines</li>
              <li>a <em>semantic web</em></li>
            </ul>
            <aside class="notes">
              HTML documents are hard for computers to understand. If web
              addresses could also serve highly structured data that machines
              could make sense of (without natural language parsing and other
              costly programming efforts), we could build all kinds of exciting
              applications!
              <p>That was Tim Berners-Lee's idea for the <em>semantic web</em>.</p>
              <p><em>Linked data</em> is the practitioner's approach to the semantic web</p>
            </aside>
          </section>
        </section>
        <section>
          <section>
            <h2>Linked data principles</h2>
            <ol>
              <li class="fragment">Use <abbr title="Uniform Resource Identifier">URI</abbr>s as names for things.</li>
              <li class="fragment">Use HTTP URIs <em class="fragment">(web addresses)</em> so that people can look up those names.</li>
              <li class="fragment">When someone looks up a URI, provide useful information, using the standards (RDF*, SPARQL)</li>
              <li class="fragment">Include links to other URIs, so that they can discover more things.</li>
            </ol>
            <hr />
            <cite class="smaller">Berners-Lee, T. (2009, June 18). Linked Data - Design Issues. Retrieved March 10, 2019, from <a href="https://www.w3.org/DesignIssues/LinkedData.html">https://www.w3.org/DesignIssues/LinkedData.html</a></cite>
            <aside class="notes">
              There are non-HTTP URIs (URN, DOI, file://, geo:), but as they
              aren't directly resolvable they inhibit the ability to easily
              publish and access their corresponding data.
            </aside>
          </section>
          <section>
            <h3>Plain language statements</h3>
            <table>
              <thead>
                <tr><th>Subject</th><th>Predicate</th><th>Object</th></tr>
              </thead>
              <tbody>
                <tr class="fragment"><td>Dan Scott</td><td>member of</td><td>Laurentian University</td></tr>
                <tr class="fragment"><td>Laurentian University</td><td>location</td><td>Sudbury</td></tr>
                <tr class="fragment"><td>Laurentian University</td><td>founding date</td><td>1960</td></tr>
              </tbody>
            </table>
            <aside class="notes">
              These are simple statements. But by combining them, we can assemble a complex body of information.
            </aside>
          </section>
          <section>
            <h3>Resource Description Framework (RDF)</h3>
            <p>Use HTTP URIs (web addresses) to identify things:</p>
            <table class="smaller">
              <thead>
                <tr><th>Subject</th><th>Predicate</th><th>Object-or-value</th></tr>
              </thead>
              <tbody>
                <tr><td>https://dscott.ca/#i</td><td>member of</td><td>Laurentian University</td></tr>
                <tr><td>Laurentian University</td><td>location</td><td>Sudbury</td></tr>
                <tr><td>Laurentian University</td><td>founding date</td><td>1960</td></tr>
              </tbody>
            </table>
          </section>
          <section>
            <h3>RDF: using HTTP URIs</h3>
            <table class="smaller">
              <thead>
                <tr><th>Subject</th><th>Predicate</th><th>Object-or-value</th></tr>
              </thead>
              <tbody>
                <tr><td>https://dscott.ca/#i</td><td>member of</td><td>https://laurentian.ca/</td></tr>
                <tr><td>https://laurentian.ca/</td><td>location</td><td>Sudbury</td></tr>
                <tr><td>https://laurentian.ca/</td><td>founding date</td><td>1960</td></tr>
              </tbody>
            </table>
          </section>
          <section>
            <h3>RDF: using HTTP URIs</h3>
            <table class="smaller">
              <thead>
                <tr><th>Subject</th><th>Predicate</th><th>Object-or-value</th></tr>
              </thead>
              <tbody>
                <tr><td>https://dscott.ca/#i</td><td>member of</td><td>https://laurentian.ca/</td></tr>
                <tr><td>https://laurentian.ca/</td><td>location</td><td>https://greatersudbury.ca/</td></tr>
                <tr><td>https://laurentian.ca/</td><td>founding date</td><td>1960</td></tr>
              </tbody>
            </table>
          </section>
          <section>
            <h3>RDF: using HTTP URIs</h3>
            <table class="smaller">
              <thead>
                <tr><th>Subject</th><th>Predicate</th><th>Object-or-value</th></tr>
              </thead>
              <tbody>
                <tr><td>https://dscott.ca/#i</td><td>http://schema.org/memberOf</td><td>https://laurentian.ca/</td></tr>
                <tr><td>https://laurentian.ca/</td><td>http://schema.org/location</td><td>https://greatersudbury.ca/</td></tr>
                <tr><td>https://laurentian.ca/</td><td>http://schema.org/foundingDate</td><td>1960</td></tr>
              </tbody>
            </table>
          </section>
          <section>
            <h3>RDF: using expressive literals</h3>
            <table class="smaller">
              <thead>
                <tr><th>Subject</th><th>Predicate</th><th>Object-or-value</th></tr>
              </thead>
              <tbody>
                <tr><td>https://dscott.ca/#i</td><td>http://schema.org/memberOf</td><td>https://laurentian.ca/</td></tr>
                <tr><td>https://laurentian.ca/</td><td>http://schema.org/location</td><td>https://greatersudbury.ca/</td></tr>
                <tr><td>https://laurentian.ca/</td><td>http://schema.org/foundingDate</td><td>"1960"^^xsd:gYear</td></tr>
                <tr class="property"><td>https://greatersudbury.ca/</td><td>http://schema.org/name</td><td>"Greater Sudbury"@en</td></tr>
                <tr class="property"><td>https://greatersudbury.ca/</td><td>http://schema.org/name</td><td>"Grand Sudbury"@fr</td></tr>
              </tbody>
            </table>
            <p class="fragment">These three-part statements are called <em>triples</em>.</p>
            <aside class="notes">
              RDF is built from statements consisting of three parts: a subject, predicate, and object.
              <p>
              Subjects and predicates have to be URIs. Note that the object of
              the first statement is the subject of the second and third
              statements.
              </p>
              <p>
              URIs play a role similar to primary keys in relational
              databases--except URIs are, well, universal.
              </p>
            </aside>
          </section>
        </section>
        <section>
          <section><h2>Vocabularies and ontologies</h2></section>
          <section>
            <h3>Vocabularies: (mostly) naming things</h3>
            <ul>
              <li><em>Classes</em> (types of things)</li>
              <li><em>Properties</em> (relationships between things, or predicates)</li>
              <li>Often expressed in <a href="https://www.w3.org/TR/rdf-schema/"><abbr title="RDF Schema">RDFS</abbr></a>:
                <ul>
                  <li>Domain and range restrictions</li>
                  <li>Human-readable descriptions</li>
                </ul>
              </li>
            </ul>
            <aside class="notes">
              RDFS is simple but very useful for describing the basic
              vocabulary.
            </aside>
          </section>
          <section>
            <h3>Ontology: "the study of being"</h3>
            <ul>
              <li>Describes a worldview for a given domain</li>
              <li>Often expressed in <a href="https://www.w3.org/TR/owl2-overview/"><abbr title="Web Ontology Language">OWL</abbr></a>
                (more complex than RDFS):
                <ul>
                  <li class="fragment smaller">Cardinalities</li>
                  <li class="fragment smaller">Class disjointness</li>
                  <li class="fragment smaller">Class intersections and unions</li>
                </ul>
              </li>
              <li class="fragment">Can link vocabularies (<code>owl:equivalentClass</code>, <code>owl:equivalentProperty</code>) and things (<code>owl:sameAs</code>)
              <li class="fragment">Enables reasoning over / deriving inferences from your data</li>
            </ul>
            <aside class="notes">
              Expressing rules gives you the ability to check the validity of
              your data and derive new knowledge from it. This is particularly
              important when you are aggregating data from multiple sources, as
              is common with Knowledge Graphs.
              <p>
              When you combine two sets of linked data, you want to map their
              schemas so that you can identify the unique triples, as well as
              identify (and resolve) potential conflicting statements.
              </p>
            </aside>
          </section>
          <section>
            <h3>Prefixes are shortcuts</h3>
            <ul>
              <li>You'll often see links like <code>rdfs:label</code> or <code>dc:creator</code></li>
              <li>"But Dan,", you say, "those aren't links!"</li>
              <li>The prefix <code>rdfs:</code> is just a shorter way of saying <code>http://www.w3.org/2000/01/rdf-schema#</code></li>
              <li><code>rdfs:name</code> really means <code>http://www.w3.org/2000/01/rdf-schema#label</code></li>
              <li><code>dc:creator</code> expands to <code>http://purl.org/dc/terms/creator</code></li>
            </ul>
            <aside class="notes">
              RDFa and JSON-LD define prefixes for <a href="https://www.w3.org/2011/rdfa-context/rdfa-1.1">many common vocabularies</a>.
              So if you don't find the prefix defined in a JSON-LD document, you can find it there.
            </aside>
          </section>
        </section>
        <section>
          <section>
            <h2>Publishing linked data</h2>
          </section>
          <section>
            <h3>Syntaxes</h3>
            <ul>
              <li>Embedded within HTML: <a href="https://json-ld.org/">JSON-LD</a>, <a href="https://rdfa.info/">RDFa</a>, microdata</li>
              <li>Parallel data (<em>content negotiation</em>): Turtle, RDF/XML, JSON-LD, NTriples, …</li>
            </ul>
            <aside class="notes">
              If you go with an inline syntax, I recommend JSON-LD. It can
              surface all of the linked data you want to publish for a given
              page within a single <code>&lt;script&gt;</code> tag. It is much
              less susceptible to breakage over time due to unrelated changes
              in HTML templates (for example, to improve the visual appeal of
              the page) that can disrupt RDFa and microdata.
              <p>
              BTW you can extract the RDFa that I embedded into the title slide
              of this presentation using <a
              href="https://search.google.com/structured-data/testing-tool#url=https%3A%2F%2Fstuff.coffeecode.net%2F2020%2Flinked_data_relational_minds">Google's Structured Data Testing Tool</a>
              </p>
            </aside>
          </section>
          <section>
            <h3>Published linked data example</h3>
            <ul>
              <li>Human-readable with inline RDFa: <a href="https://dscott.ca/#i">https://dscott.ca/#i</a> - <a href="http://linter.structured-data.org/?url=https:%2F%2Fdscott.ca%2F#i">(extract)</a></li>
              <li>Turtle (Terse Triple Language): <a href="https://dscott.ca/index.ttl">https://dscott.ca/#i</a></li>
              <li>JSON-LD: <a href="https://dscott.ca/index.json">https://dscott.ca/#i</a></li>
              <li>RDF/XML: <a href="https://dscott.ca/index.rdf">https://dscott.ca/#i</a></li>
            </ul>
        </section>
        <section>
          <h3>Knowledge graphs</h3>
          <ul>
            <li>But crawling the web is a lot of work!</li>
            <li>A <em>knowledge graph</em> is a set of queryable linked data, organized via an ontology, that represents entities and their relationships</li>
            <li>Google Knowledge Graph</li>
            <li>DBPedia: linked data extracted from Wikipedia</li>
            <li>Wikidata: linked data supporting all Wikimedia Foundation products</li>
          </ul>
        </section>
      </section>
      <section>
        <section>
          <h2>Querying linked data: <abbr title="SPARQL Protocol and Query Language">SPARQL</abbr></h2>
          <ul>
            <li>Just different enough from SQL to be confusing 😕
              <ul>
                <li><code>SELECT</code> variables rather than columns</li>
                <li>No <code>FROM</code> clause: just the entire dataset of triples!</li>
                <li><code>WHERE</code> clause creates a pattern for the triples you want</li>
                <li>No <code>JOIN</code>s: relationships between entities</li>
                <li><code>FILTER</code> attribute values to narrow further</li>
              </ul>
            </li>
          </ul>
        </section>
        <section>
          <h2>Hands-on with Wikidata: randomness</h2>
          <ol>
            <li>Open the <a href="https://query.wikidata.org/">Wikidata Query Service (WDQS)</a> in a browser.</li>
            <li>On the right hand side, type the following query:
              <pre><code>SELECT * WHERE {
?s ?p ?o
}
LIMIT 10</code></pre></li>
              <li>Press <code>CTRL + ENTER</code>, or click the arrow button on the left, to submit the query.</li>
          </ol>
          <aside class="notes">
            Wikidata is one of the largest sets of openly queryable RDF data.
            It has a rich ontology with thousands of classes and properties.
            <p>
            Note, however, the data it contains may not be complete, current,
            or accurate. (This is arguably true of any large-scale store of
            data!)
            </p>
          </aside>
        </section>
        <section>
          <h2>Wikidata SPARQL: Laurentian</h2>
          <ol>
            <li>Let's get something less random. Change your object from <code>?o</code> to <code>"Laurentian University"</code>: 
              <a href="https://query.wikidata.org/#SELECT%20%2a%20WHERE%20%7B%0A%20%20%3Fs%20%3Fp%20%22Laurentian%20University%22%0A%7D%0ALIMIT%2010"><pre><code>SELECT * WHERE {
  ?s ?p "Laurentian University"
}
LIMIT 10</code></pre></a></li>
              <li>One of the subjects should be <a href="http://www.wikidata.org/entity/Q3551432">Q3551432</a>. Click it to see the structured data about Laurentian University.</li>
          </ol>
          <aside class="notes">
            Wikidata uses Q-numbers instead of "natural keys" to identify
            entities because it wants to support many different languages.
            <p>
            The first statement for Laurentian was <a href="https://www.wikidata.org/wiki/Property:P31">"instance of (P31)"</a> <a href="https://www.wikidata.org/wiki/Q108403040">university in Ontario (Q108403040)"</a>.
            </p>
          </aside>
        </section>
        <section>
          <h2>Wikidata SPARQL: Universities</h2>
          Let's retrieve all instances of universities in Wikidata:
          <ol>
            <li>Change your predicate from <code>?p</code> to <code>wdt:P31</code> (<em>instance of</em>).</li>
            <li>Change your object from <code>"Laurentian University"</code> to <code>wd:Q3918</code> (<em>university</em>).
              <a href="https://query.wikidata.org/#SELECT%20%2a%20WHERE%20%7B%0A%20%20%3Fs%20wdt%3AP31%20wd%3AQ3918%20%23%20instance%20of%20university%0A%7D"><pre><code>SELECT * WHERE {
  ?s wdt:P31 wd:Q3918 # instance of university
}</code></pre></a></li>
          </ol>
          <ul>
            <li><code>wdt:</code> = "truthiness"</li>
            <li><code>wd:</code> = entity</li>
          </ul>
        </section>
        <section>
          <h2>Wikidata SPARQL: countries</h2>
          Let's get the countries for each university in Wikidata:
          <ol>
            <li>Append the "AND" operator (a period <code>.</code>) to your first statement.</li>
            <li>Add a statement that asks for the country (predicate = <code>wdt:P17</code>) for the subject and store the value in a new variable, <code>?country</code>.
              <a href="https://query.wikidata.org/#SELECT%20%2a%20WHERE%20%7B%0A%20%20%3Fs%20wdt%3AP31%20wd%3AQ3918%20.%0A%20%20%3Fs%20wdt%3AP17%20%3Fcountry%0A%7D"><pre><code>SELECT * WHERE {
  ?s wdt:P31 wd:Q3918 .
  ?s wdt:P17 ?country
}</code></pre></a></li>
          </ol>
        </section>
        <section>
          <h2>Wikidata SPARQL: labels!</h2>
          Okay, we need human-friendly labels.
          <ol>
            <li>Ask for the <code>rdfs:label</code> of the subject.</li>
            <li>Ask for the <code>rdfs:label</code> of the country.</li>
            <li>Add a <code>LIMIT 100</code> clause to keep things fast.
              <a href="https://query.wikidata.org/#SELECT%20%2a%20WHERE%20%7B%0A%20%20%3Fs%20wdt%3AP31%20wd%3AQ3918%20.%0A%20%20%3Fs%20wdt%3AP17%20%3Fcountry%20.%0A%20%20%3Fs%20rdfs%3Alabel%20%3FsLabel%20.%0A%20%20%3Fcountry%20rdfs%3Alabel%20%3FcountryLabel%20.%0A%7D%0ALIMIT%20100"><pre><code>SELECT * WHERE {
  ?s wdt:P31 wd:Q3918 .
  ?s wdt:P17 ?country .
  ?s rdfs:label ?sLabel .
  ?country rdfs:label ?countryLabel .
}
LIMIT 100</code></pre></a></li>
          </ol>
          <p>Don't forget the "AND" operator (period <code>.</code>) for your statements!</p>
        </section>
        <section>
          <h2>Wikidata SPARQL: English labels!</h2>
          <ol>
            <li>Add FILTER statements that restrict the language of each label to "en" (<code>FILTER(LANG(?countryLabel) = "en")</code>)
              <a href="https://query.wikidata.org/#SELECT%20%2a%20WHERE%20%7B%0A%20%20%3Fs%20wdt%3AP31%20wd%3AQ3918%20.%0A%20%20%3Fs%20wdt%3AP17%20%3Fcountry%20.%0A%20%20%3Fs%20rdfs%3Alabel%20%3FsLabel%20.%0A%20%20%3Fcountry%20rdfs%3Alabel%20%3FcountryLabel%20.%0A%20%20FILTER%28LANG%28%3FsLabel%29%20%3D%20%22en%22%29%20.%0A%20%20FILTER%28LANG%28%3FcountryLabel%29%20%3D%20%22en%22%29%20.%0A%7D%0ALIMIT%20100"><pre><code>SELECT * WHERE {
  ?s wdt:P31 wd:Q3918 .
  ?s wdt:P17 ?country .
  ?s rdfs:label ?sLabel .
  ?country rdfs:label ?countryLabel .
  FILTER(LANG(?sLabel) = "en") .
  FILTER(LANG(?countryLabel) = "en") .
}
LIMIT 100</code></pre></a></li>
          </ol>
          <aside class="notes">
            Wikidata has an extension to SPARQL called a label service that
            is faster and easily supports multiple languages, but this works
            as a decent example for using FILTER.
          </aside>
        </section>
        <section>
          <h2>Wikidata SPARQL: English labels!</h2>
          <ol>
            <li>Replace the FILTER statements with Wikidata's fast label service:
              <a href="https://query.wikidata.org/#SELECT%20%2a%20WHERE%20%7B%0A%20%20%3Fs%20wdt%3AP31%20wd%3AQ3918%20.%0A%20%20%3Fs%20wdt%3AP17%20%3Fcountry%20.%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22%5BAUTO_LANGUAGE%5D%2Cen%22.%20%7D%0A%7D%0ALIMIT%20100"><pre><code>SELECT * WHERE {
  ?s wdt:P31 wd:Q3918 .
  ?s wdt:P17 ?country .
  SERVICE wikibase:label { 
    bd:serviceParam wikibase:language "en".
  }
}
LIMIT 100</code></pre></a></li>
          </ol>
        </section>
        <section>
          <h2>SPARQL semi-colon operator</h2>
          <ol>
            <li>To avoid repeating the subject (<code>?s</code>) in successive statements, use a semi-colon <code>;</code> instead of the period (<code>.</code>):
              <a href="https://query.wikidata.org/#SELECT%20%2a%20WHERE%20%7B%0A%20%20%3Fs%20wdt%3AP31%20wd%3AQ3918%3B%0A%20%20%20%20%20wdt%3AP17%20%3Fcountry%20.%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%0A%20%20%20%20bd%3AserviceParam%20wikibase%3Alanguage%20%22en%22.%0A%20%20%7D%0A%7D%0ALIMIT%20100%0A"><pre><code>SELECT * WHERE {
  ?s wdt:P31 wd:Q3918;
     wdt:P17 ?country .
  SERVICE wikibase:label { 
    bd:serviceParam wikibase:language "en".
  }
}
LIMIT 100</code></pre></a></li>
          </ol>
        </section>
        <section>
          <h2>UNION: the OR operator</h2>
          <ol>
            <li>Until now, you've been strictly using <code>AND</code> expressions. But the <code>UNION</code> operator allows you to create OR expressions. For example, to list universities in Ontario or Québec:
              <a href="https://query.wikidata.org/#SELECT%20%3Fs%20%3FsLabel%20WHERE%20%7B%0A%20%20%7B%20%3Fs%20wdt%3AP31%20wd%3AQ108403040%20%7D%20%20%23%20university%20in%20Ontario%0A%20%20UNION%0A%20%20%7B%20%3Fs%20wdt%3AP31%20wd%3AQ3551519%20%7D%20.%20%20%23%20university%20in%20Quebec%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%0A%20%20%20%20bd%3AserviceParam%20wikibase%3Alanguage%20%22en%22.%0A%20%20%7D%0A%7D%0ALIMIT%20100"><pre><code>SELECT ?s ?sLabel WHERE {
  { ?s wdt:P31 wd:Q108403040 }  # university in Ontario
  UNION
  { ?s wdt:P31 wd:Q3551519 } .  # university in Quebec
  SERVICE wikibase:label {
    bd:serviceParam wikibase:language "en".
  }
}
LIMIT 100</code></pre></a></li>
          </ol>
        </section>
      </section>
      <section>
        <section><h2>Advanced SPARQL</h2></section>
        <section>
          <h2>SPARQL: ORDER</h2>
          Sort the results by country name.
          <ol>
            <li>Add an <code>ORDER BY ?countryLabel</code> clause just before the <code>LIMIT</code> clause:
              <a href="https://query.wikidata.org/#SELECT%20%3Fs%20%3FsLabel%20%3Fcountry%20%3FcountryLabel%20WHERE%20%7B%0A%20%20%3Fs%20wdt%3AP31%20wd%3AQ3918%20.%0A%20%20%3Fs%20wdt%3AP17%20%3Fcountry%20.%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%0A%20%20%20%20bd%3AserviceParam%20wikibase%3Alanguage%20%22en%22.%0A%20%20%7D%0A%7D%0AORDER%20BY%20%3FcountryLabel%0ALIMIT%20100"><pre><code>SELECT ?s ?sLabel ?country ?countryLabel WHERE {
  ?s wdt:P31 wd:Q3918 .
  ?s wdt:P17 ?country .
  SERVICE wikibase:label {
    bd:serviceParam wikibase:language "en".
  }
}
ORDER BY ?countryLabel
LIMIT 100</code></pre></a></li>
          </ol>
        </section>
        <section>
          <h2>Wikidata SPARQL: on a map</h2>
          <ol>
            <li>Add a clause requesting <code>P625</code> (coordinates) and store it in a <code>?coords</code> variable:
<a href="https://query.wikidata.org/#%23defaultView%3AMap%0ASELECT%20%3Fs%20%3FsLabel%20%3Fcountry%20%3FcountryLabel%20%3Fcoords%20WHERE%20%7B%0A%20%20%3Fs%20wdt%3AP31%20wd%3AQ3918%20.%0A%20%20%3Fs%20wdt%3AP17%20%3Fcountry%20.%0A%20%20%3Fs%20wdt%3AP625%20%3Fcoords%20.%20%23%20give%20us%20coordinates%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%0A%20%20%20%20bd%3AserviceParam%20wikibase%3Alanguage%20%22en%22.%0A%20%20%7D%0A%7D%0AORDER%20BY%20%3FcountryLabel%0ALIMIT%20100"><pre><code>SELECT ?s ?sLabel ?country ?countryLabel ?coords WHERE {
  ?s wdt:P31 wd:Q3918 .
  ?s wdt:P17 ?country .
  ?s wdt:P625 ?coords . # give us coordinates
  SERVICE wikibase:label {
    bd:serviceParam wikibase:language "en".
  }
}
ORDER BY ?countryLabel
LIMIT 100</code></pre></a>
            </li>
            <li>Change the display type from the standard table to a map by clicking the eye icon</li>
          </ol>
          <aside class="notes">
            This has the effect of filtering out any universities that do not have coordinates. It's like an INNER JOIN in SQL.
          </aside>
        </section>
        <section>
          <h2>Wikidata SPARQL: subclasses and OPTIONAL</h2>
          <p>Let's focus on Canadian universities</p>
          <a href="https://query.wikidata.org/#%23%20show%20instances%20%26%20subclasses%20of%20Canadian%20universities%20on%20a%20map%0A%23defaultView%3AMap%0A%0ASELECT%20%2a%20WHERE%20%7B%0A%20%20%3Fs%20wdt%3AP31%2Fwdt%3AP279%2a%20wd%3AQ3918%20.%20%23%20instances%20of%20and%20subclasses%20of%20university%0A%20%20%3Fs%20wdt%3AP31%20%3FinstanceOf%20.%20%20%20%20%20%20%20%20%23%20instance%20of%20what%3F%0A%20%20%3Fs%20wdt%3AP17%20wd%3AQ16%20.%20%20%20%20%20%20%20%20%20%20%20%20%20%23%20in%20Canada%0A%20%20%3Fs%20rdfs%3Alabel%20%3FsLabel%20.%0A%20%20%3FinstanceOf%20rdfs%3Alabel%20%3FinstanceLabel%20.%0A%20%20FILTER%28LANG%28%3FsLabel%29%20%3D%20%22en%22%29%20.%20%20%23%20give%20us%20the%20English%20label%0A%20%20FILTER%28LANG%28%3FinstanceLabel%29%20%3D%20%22en%22%29%20.%20%20%23%20give%20us%20the%20English%20label%0A%20%20OPTIONAL%7B%3Fs%20wdt%3AP625%20%3Fcoords%7D%20.%20%23%20and%20coordinates%2C%20if%20possible%0A%7D%20ORDER%20BY%20%3FsLabel%0A"><pre><code># show instances & subclasses of Canadian universities on a map
#defaultView:Map

SELECT * WHERE {
  ?s wdt:P31/wdt:P279* wd:Q3918 . # instances of and subclasses of university
  ?s wdt:P31 ?instanceOf .        # instance of what?
  ?s wdt:P17 wd:Q16 .             # in Canada
  ?s rdfs:label ?sLabel .
  ?instanceOf rdfs:label ?instanceLabel .
  FILTER(LANG(?sLabel) = "en") .  # give us the English label
  FILTER(LANG(?instanceLabel) = "en") .  # give us the English label
  OPTIONAL{?s wdt:P625 ?coords} . # and coordinates, if possible
}
ORDER BY ?sLabel</code></pre></a>
            <aside class="notes">If we switch back to the Table view, we can easily see universities lacking coordinates. And fix them!</aside>
        </section>
        <section>
          <h2>SPARQL: aggregates</h2>
          SPARQL can count and group too:
          <ol>
            <li>Change your <code>SELECT</code> clause to select <code>?countryLabel (COUNT (?s) AS ?cnt)</code>.</li>
            <li>Add an <code>GROUP BY DESC(?cnt)</code> clause just before the <code>ORDER BY</code> clause:
              <a href="https://query.wikidata.org/#SELECT%20%3FcountryLabel%20%28COUNT%20%28%3Fs%29%20AS%20%3Fcnt%29%20WHERE%20%7B%0A%20%20%3Fs%20wdt%3AP31%20wd%3AQ3918%20.%0A%20%20%3Fs%20wdt%3AP17%20%3Fcountry%20.%0A%20%20%3Fcountry%20rdfs%3Alabel%20%3FcountryLabel%20.%0A%20%20FILTER%28LANG%28%3FcountryLabel%29%20%3D%20%22en%22%29%20.%0A%7D%0AGROUP%20BY%20%3FcountryLabel%0AORDER%20BY%20DESC%28%3Fcnt%29%0ALIMIT%20100"><pre><code>SELECT ?countryLabel (COUNT (?s) AS ?cnt) WHERE {
  ?s wdt:P31 wd:Q3918 .
  ?s wdt:P17 ?country .
  ?country rdfs:label ?countryLabel .
  FILTER(LANG(?countryLabel) = "en") .
}
GROUP BY ?countryLabel
ORDER BY DESC(?cnt)
LIMIT 100</code></pre></a></li>
          </ol>
        </section>
        <section>
          <h2>SPARQL: aggregate functions</h2>
          <ol>
            <li>The aggregate functions defined in SPARQL 1.1 are COUNT, SUM, MIN, MAX, AVG, GROUP_CONCAT, and SAMPLE</li>
            <li>GROUP_CONCAT takes a variable name and the string with which you want to concatenate each value of the variable, e.g. <code>GROUP_CONCAT(?instrument, "; ")</code></li>
          </ol>
        </section>
        <section>
          <h2>Wikidata SPARQL: HAVING filter</h2>
          SPARQL can filter aggregate results
          <ol>
            <li>Add a <code>HAVING(COUNT(?s) &lt;= 50)</code> clause just before the <code>ORDER BY</code> clause:
              <a href="https://query.wikidata.org/#SELECT%20%3FcountryLabel%20%28COUNT%20%28%3Fs%29%20AS%20%3Fcnt%29%20%3Fpopulation%20WHERE%20%7B%0A%20%20%3Fs%20wdt%3AP31%20wd%3AQ3918%20.%0A%20%20%3Fs%20wdt%3AP17%20%3Fcountry%20.%0A%20%20%3Fcountry%20rdfs%3Alabel%20%3FcountryLabel%20.%0A%20%20%3Fcountry%20wdt%3AP1082%20%3Fpopulation%20.%0A%20%20FILTER%28LANG%28%3FcountryLabel%29%20%3D%20%22en%22%29%20.%0A%7D%0AGROUP%20BY%20%3FcountryLabel%20%3Fpopulation%0AHAVING%28COUNT%28%3Fs%29%20%3C%3D%2050%29%0AORDER%20BY%20DESC%28%3Fcnt%29%0ALIMIT%20100"><pre><code>SELECT ?countryLabel (COUNT (?s) AS ?cnt) WHERE {
?s wdt:P31 wd:Q3918 .
?s wdt:P17 ?country .
?country rdfs:label ?countryLabel .
FILTER(LANG(?countryLabel) = "en") .
}
GROUP BY ?countryLabel
HAVING(COUNT(?s) &lt;= 50)
ORDER BY DESC(?cnt)
LIMIT 100</code></pre></a></li>
          </ol>
        </section>
        <section>
          <h2>SPARQL: CONSTRUCT graphs</h2>
          <ul>
            <li><code>SELECT</code> returns tabular data. <code>CONSTRUCT</code> returns RDF graphs:
<a href="https://query.wikidata.org/#CONSTRUCT%20%7B%20%0A%20%20%3Fs%20wdt%3AP625%20%3Fcoords%20.%0A%20%20%3Fs%20wdt%3AP17%20%3Fcountry%20.%0A%7D%20WHERE%20%7B%0A%20%20%3Fs%20wdt%3AP31%20wd%3AQ3918%3B%0A%20%20%20%20%20wdt%3AP17%20%3Fcountry%3B%0A%20%20%20%20%20wdt%3AP625%20%3Fcoords%20.%20%23%20give%20us%20coordinates%0A%7D"><pre><code>CONSTRUCT { 
  ?s wdt:P625 ?coords .
  ?s wdt:P17 ?country .
} WHERE {
  ?s wdt:P31 wd:Q3918;
     wdt:P17 ?country;
     P625 ?coords . # give us coordinates
}</code></pre></a></li>
            <li>WDQS displays the data in tabular format for humans. Click <b>Code</b> for code snippets in various programming languages.</li>
          </ul>
        </section>
      </section>
      <section>
        <section><h2>Hands on app time!</h2></section>
        <section>
          <h3>Wikidata-powered infocards</h3>
          <ul>
            <li>Years ago, I taught our Evergreen library catalogue to display Wikidata infocards about musicians and bands</li>
            <li>Sadly we migrated from Evergreen in 2020</li>
            <li>But the <a href="https://gitlab.com/denials/wikidata-music-infocard">generic code</a> is still available</li>
            <li>The following lab is a simplified version of that code</li>
          </ul>
        </section>
        <section>
          <h3>Get the code and try it out</h3>
          <ul>
            <li>Open <a href="https://stuff.coffeecode.net/2022/swib_sparql_workshop/code">https://stuff.coffeecode.net/2022/swib_sparql_workshop/code</a>
              in your browser</li>
            <li>Download the files</li>
            <li>Start up a web server in the directory with the files:
              <ul>
                <li>php -S localhost:8000</li>
                <li>python -m http.server</li>
                <li>npx http-server -p 8000</li>
                <li>An actual web server</li>
              </ul>
            </li>
            <li>Point your web browser at <a href="http://localhost:8000/">http://localhost:8000/</a></li>
          </ul>
        </section>
        <section>
          <h3>Progressively enhance</h3>
          <ul>
            <li>We start basic: just a list of pets with their Wikidata concept URIs</li>
            <li>At each stage, I add a chunk of vanilla JavaScript
              <ul>
                <li>Simple DOM manipulation</li>
                <li>Iteratively building the SPARQL query</li>
                <li>Parsing and displaying the results (<em>entity summarization</em>)</li>
              </ul>
            </li>
            <li>Get your hands dirty! Get more data, or try a different dataset!</li>
          </ul>
        </section>
      </section>

			</div>
		</div>

		<script src="dist/reveal.js"></script>
		<script src="plugin/notes/notes.js"></script>
		<script src="plugin/highlight/highlight.js"></script>
		<script>
			// More info about initialization & config:
			// - https://revealjs.com/initialization/
			// - https://revealjs.com/config/
			Reveal.initialize({
				hash: true,

				// Learn about plugins: https://revealjs.com/plugins/
				plugins: [ RevealHighlight, RevealNotes ]
			});
		</script>
	</body>
</html>
